module github.com/pocockn/mono-repo

go 1.18

require (
	github.com/BurntSushi/toml v1.1.0
	github.com/golang/mock v1.4.3
	github.com/jinzhu/gorm v1.9.12
	github.com/labstack/echo/v4 v4.7.2
	github.com/labstack/gommon v0.3.1
	github.com/pkg/errors v0.9.1
	github.com/pocockn/awswrappers v0.0.0-20190604072748-a1628bbacc94
	github.com/pocockn/models v0.8.0
	github.com/pocockn/recs-api v0.1.0
	github.com/rs/zerolog v1.27.0
	github.com/rzajac/zltest v0.12.0
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.5.0
	github.com/stretchr/testify v1.7.0
	github.com/zmb3/spotify v0.0.0-20200331200324-6a9312f5d1de
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	gopkg.in/gormigrate.v1 v1.6.0
)

require (
	github.com/aws/aws-sdk-go v1.20.20 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-sql-driver/mysql v1.4.1 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/golang/protobuf v1.2.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jmespath/go-jmespath v0.0.0-20180206201540-c2b33e8439af // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.1 // indirect
	github.com/labstack/echo v3.3.10+incompatible // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.1 // indirect
	github.com/vidsy/go-kmsconfig v0.0.0-20190205094205-1627843c4cce // indirect
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5 // indirect
	golang.org/x/net v0.0.0-20211015210444-4f30a5c0130f // indirect
	golang.org/x/sys v0.0.0-20211103235746-7861aae1554b // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/time v0.0.0-20201208040808-7e3f01d25324 // indirect
	google.golang.org/appengine v1.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
